﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
public class PlayerManager : MonoBehaviour
{
    Animator anim;
    private SpriteRenderer mySpriteRenderer;
    public float speed = 50, running = 100, jumpHeight;
    public bool inAir, flipX, ispressed = false;
    Rigidbody2D myBody;
    // Use this for initialization

   void Start()
    {
        myBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
    }
    void Update()
        {   
        //jumping
        if (!inAir && (CrossPlatformInputManager.GetButton("jump")))
        { 
            myBody.AddForce(new Vector2(myBody.velocity.x, jumpHeight));
            anim.SetInteger("Movement", 3);
            inAir = true;
        }
        // Standing
        if (CrossPlatformInputManager.GetAxis("Horizontal") == 0 && !inAir) 
        {
            anim.SetInteger("Movement", 0);
        }
        //running right
        if (CrossPlatformInputManager.GetAxis("Horizontal") <= 1 && CrossPlatformInputManager.GetAxis("Horizontal") > .5)
        {
            mySpriteRenderer.flipX = false;
            anim.SetInteger("Movement", 2);
            myBody.position += Vector2.right * speed * 2 * Time.deltaTime; 
        }
        //running left 
        if (CrossPlatformInputManager.GetAxis("Horizontal") >= -1 && CrossPlatformInputManager.GetAxis("Horizontal") < -.5)
        {
            mySpriteRenderer.flipX = true;
            anim.SetInteger("Movement", 2);
            myBody.position += Vector2.left * speed * 2 * Time.deltaTime;
        }
        //walking left
        if (CrossPlatformInputManager.GetAxis("Horizontal") >= -.5 && CrossPlatformInputManager.GetAxis("Horizontal") < 0)
        {
            mySpriteRenderer.flipX = true;
            anim.SetInteger("Movement", 1);
            myBody.position += Vector2.left * speed * Time.deltaTime;
        }
        //walking right
        if (CrossPlatformInputManager.GetAxis("Horizontal") <= .5 && CrossPlatformInputManager.GetAxis("Horizontal") > 0)
        {
            mySpriteRenderer.flipX = false;
            anim.SetInteger("Movement", 1);
            myBody.position += Vector2.right * speed  * Time.deltaTime;
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        inAir = false;
    }

 
 /*   void Start()
    { 
        anim = GetComponent<Animator>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        myBody = GetComponent<Rigidbody2D>();

    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        inAir = false;
    }
    // Update is called once per frame

    void Update()
    {

        
        if (!inAir && (Input.GetKey(KeyCode.Space)))
            { 
            inAir = true;
            anim.SetInteger("Movement", 3);
            myBody.AddForce(new Vector2(myBody.velocity.x, jumpHeight));
        }
        if (ispressed == false)
        {
            anim.SetInteger("Movement", 0);
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {

            anim.SetInteger("Movement", 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            mySpriteRenderer.flipX = false;
            anim.SetInteger("Movement", 1);
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
        if (Input.GetKeyUp(KeyCode.D))
        {

            anim.SetInteger("Movement", 0);


        }
        if (Input.GetKey(KeyCode.A))
        {
            mySpriteRenderer.flipX = true;
            anim.SetInteger("Movement", 1);
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
        if (Input.GetKeyUp(KeyCode.A))
        {

            anim.SetInteger("Movement", 0);

        }

        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetInteger("Movement", 2);
            transform.position += Vector3.right * speed * 2 * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetInteger("Movement", 2);
            transform.position += Vector3.left * speed * 2 * Time.deltaTime;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKey(KeyCode.A))
            {
                anim.SetInteger("Movement", 1);
            }
            else { anim.SetInteger("Movement", 0); }
        }
        
    }
#endif
    */
}





